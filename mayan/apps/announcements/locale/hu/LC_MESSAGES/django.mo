��             +         �  ,   �  -   �     $  /   1     a     v     �     �     �  >   �  >         ?      F     g  +   |  ,   �     �     �     �            9        S     Y     t  '   y     �     �      �     �     �  e  �  *   T  *        �  /   �     �     �          $     >  9   Z  5   �     �  #   �     �  .   	  .   >	     m	     z	     �	     �	     �	  =   �	     
  (   $
     M
     S
     s
     �
  (   �
     �
     �
                 	                                                                                                                           
    %(count)d announcement deleted successfully. %(count)d announcements deleted successfully. Announcement Announcement "%(object)s" deleted successfully. Announcement created Announcement edited Announcements Create announcement Create announcements Date and time after which this announcement will be displayed. Date and time until when this announcement is to be displayed. Delete Delete announcement: %(object)s. Delete announcements Delete the %(count)d selected announcement. Delete the %(count)d selected announcements. Edit Edit announcement: %s Edit announcements Enabled End date time Error deleting announcement "%(instance)s"; %(exception)s Label No announcements available None Short description of this announcement. Start date time Text The actual text to be displayed. URL View announcements Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-02 18:58+0000
Last-Translator: Csaba Tarjányi, 2024
Language-Team: Hungarian (https://app.transifex.com/rosarior/teams/13584/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 %(count)d közlemény sikeresen törölve. %(count)d közlemény sikeresen törölve. Közlemény A "%(object)s" közlemény sikeresen törölve. Közlemény létrehozva Közlemény szerkesztve Közlemények Közlemény létrehozása Közlemények létrehozása Dátum és idő, amely után ez a közlemény megjelenik. Dátum és idő, ameddig ez a közlemény megjelenik. Törlés Közlemény törlése: %(object)s . Közlemények törlése %(count)d kiválasztott közlemény törlése. %(count)d kiválasztott közlemény törlése. Szerkesztés Közlemény szerkesztése: %s Közlemények szerkesztése Engedélyezett Befejezés dátuma és ideje Hiba a "%(instance)s" közlemény törlésekor; %(exception)s Cimke Nem állnak rendelkezésre közlemények Nincs A közlemény rövid leírása. Kezdés dátuma és ideje Szöveg A ténylegesen megjelenítendő szöveg. URL Közlemények megtekintése 