��    `        �         (  "   )     L     R  
   Z     e  ;   s  	   �  
   �     �     �  I   �  <   "	     _	  "   e	     �	     �	  0   �	  H   �	     %
     ,
     =
     K
     P
      d
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
       	     F     I   _  	   �     �  
   �     �     �  B   �            5   .     d     u     �     �     �     �  H   �               7  
   D     O  U   V  `   �  7     &   E  &   l     �     �     �     �     �     �  K   �     2  !   F  o   h     �     �     �       �     V   �     �                    &     9     M     U     j  +   p     �     �  5   �     �     �     �  �    -   �  
                  *  F   F  !   �  )   �  '   �  
     ]     Q   j  
   �  #   �  %   �  )     0   ;  _   l  
   �      �     �  
     )   "  :   L      �     �  %   �  )   �          +     4     Q  E   a     �     �  6   �  Y   �     L     Z     _     s     �  E   �     �     �  5   	     ?     Y     k  $   �  
   �     �  �   �     R  %   r     �  
   �  
   �  K   �  i     9   v  &   �  "   �     �          )     F  2   e     �  c   �       *   +     V      �     �  '        >  �   T  f   �     f       }   
   �      �      �       �      !  -   !     ?!  ;   L!      �!     �!  :   �!      �!  1   
"     <"                ]   2   6   "   +   8               )       C       F      U   -   `      Q   O              &      .   \   4   ,                 @           9      7         N   /   J   (           '       1   R      Z           M   !   #          ?   G             K          <          Y             
   P   :   %      0   B       =   3      D   _       W   ;   S          V          E       X   5       ^            L   H   [   A   $                 	           >      I   *               T    A short text describing the theme. About Actions Appearance Are you sure? Before you can fully use Mayan EDMS you need the following: Bootstrap Bootswatch Bulk actions Cancel Check the logs for messages with the tags "ERROR", "CRITICAL" or "FATAL". Check you network connection and try again in a few moments. Close Contact your system administrator. Create new theme Create new themes Custom HTTP response code for AJAX redirections. Delay in milliseconds after which the menus will be checked for updates. Delete Delete theme: %s Delete themes Edit Edit theme settings Edit theme settings for user: %s Edit theme: %s Edit themes Fancybox FontAwesome Getting started Identifier Insufficient permission JQuery Form JQuery Match Height Label Lato font Maximum number of characters that will be displayed as the view title. Maximum number of requests that can be made before throttling is enabled. Next page No No results None Object Open a ticket using the <a href="%(issue_url)s">issue tracker</a>. Open main menu Page not found Position where the system messages will be displayed. Possible reasons Previous page Refresh the page. Released under the license: Save Select 2 Select items to activate bulk actions. Use Shift + click to select many. Select/Deselect all Server communication error Server error Stylesheet Submit The CSS stylesheet to change the appearance of the different user interface elements. The location is be correct, but your account does not have the required permission to access it. The location is correct, but the item no longer exists. The location of the item is incorrect. The requested page could not be found. Theme Theme created Theme edited Theme settings Theme settings for user: %s Themes Themes allow changing the visual appearance without requiring code changes. There are no themes There's been an unexpected error. Time in milliseconds after which a throttled request will clear allowing an additional request to be performed. Toastr Toggle Dropdown Toggle list display mode Toggle navigation Too many pending requests. Additional requests will be blocked until a pending one is completed or after the cooling off period ends. Total (%(start)s - %(end)s out of %(total)s) (Page %(page_number)s of %(total_pages)s) Total: %(total)s URI.js User User theme edited User theme setting User theme settings Version View existing themes Views Wait a bit. The system might be overloaded. What to do next? Yes You don't have enough permissions for this operation. jQuery jQuery Lazy Load required Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-02 18:58+0000
Last-Translator: Ovadia Ovadia, 2024
Language-Team: Hebrew (Israel) (https://app.transifex.com/rosarior/teams/13584/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 מלל קצר לתאור ערכת הנושא. אודות פעולות ניראות  האם את/ה בטוח/ה? לפני שאפשר להשתמש ב Mayan EDMS במלואו, נדרש: Bootstrap (סביבת עבודה) Bootswatch (מאגר ערכות נושא) פעולות על מספר פריטים ביטול לחפש ביומני המערכת (logs) את המילים "ERROR" ,"CRITICAL" או "FATAL". לבדוק את חיבור הרשת ולנסות שוב בעוד כמה דקות. סגירה התקשר למנהל המערכת. יצירת ערכת נושא חדשה יצירת ערכות נושא חדשות Custom HTTP response code for AJAX redirections. העיכוב (באלפיות השנייה) שאחריו יבדקו עדכוני תפריטים. מחיקה מחיקת ערכת נושא: %s מחיקת ערכות נושא עריכה עריכת הגדרות ערכת נושא עריכת הגדרות ערכת נושא למשתמש: %s עריכת ערכת נושא: %s עריכת ערכות נושא Fancybox (כלי תצוגת תוכן) FontAwesome (ספריית סמלילים) בואו נתחיל מזהה הרשאה לא מספיקה טופס JQuery כלי להתאמת גודל אובייקטים (JQuery Match Height) Label גופן Lato מספר תווים מירבי לכותרת תצוגה מספר מירבי של בקשות בטרם אפשר למערכת להאט (throttling). הדף הבא לא אין תוצאות אף אחד/כלום רכיב פתיחת כרטיס עם <a href="%(issue_url)s"> issue tracker</a> פתיחת תפריט ראשי דף לא נמצא מיקום התצוגה של הודעות מערכת. סיבות אפשריות הדף הקודם לרענן את הדף. משוחרר תחת הרישיון:  שמירה בחר 2 בחירת פריטים בכדי להפעיל פעולות על קבוצת פריטים. להשתמש ב-Shift ו<קליק> לבחירה מרובה.  בחר/בטל בחירת הכל תקלה בתקשורת עם השרת שגיאת שרת Stylesheet שליחה ה-CSS לשינוי הניראות של מרכיבי ממשק המשתמש. המיקום נכון, אך לקוד המשתמש אין הרשאות הנדרשות לגישה אליו. המיקום נכון, אך הפריט אינו קיים. מיקום הפריט הוא שגוי. הדף המבוקש לא נמצא. ערכת נושא ערכת נושא נוצרה ערכת נושא נערכה הגדרות ערכת נושא הגדרות ערכת נושא של משתמש: %s ערכות נושא ערכות נושא מאפשרות שינוי הניראות ללא צורך בשינויי קוד. אין ערכות נושא אירעה שגיאה בלתי צפויה. זמן (באלפיות השנייה) שאחריו בקשה מעוכבת תשוחרר לטובת ביצוע בקשה נוספת. ספריית Javascript (Toastr) החלף רשימת בחירה החלפת מצב תצוגת רשימה החלפת ניתוב יותר מדי בקשות ממתינות. בקשות נוספות ייחסמו עד אשר בקשה ממתינה תבוצע או אחרי סיום תקופת צינון. בסך הכל (%(start)s - %(end)s מתוך %(total)s) (דף %(page_number)s מתוך %(total_pages)s) סך הכל: %(total)s ספריית Javascript (URI.js) משתמש ערכת משתמש נערכה הגדרת ערכת משתמש הגדרות ערכת משתמש גרסה צפייה בערכות נושא קיימות תצוגות המתן רגע. המערכת כנראה בעומס יתר. מה הדבר הבא לעשות? כן אין לך הרשאות מספיקות לפעולה זו. jQuery (ספריית JavaScript) תוסף לטעינת תוכן (jQuery Lazy Load) נדרש 